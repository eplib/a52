<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A52401">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>Orders to be observed on the day of the royal coronation of King William and Queen Mary the eleventh of this instant April.</title>
    <author>Norfolk, Henry Howard, Duke of, 1655-1701.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A52401 of text R38117 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing N1234). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A52401</idno>
    <idno type="STC">Wing N1234</idno>
    <idno type="STC">ESTC R38117</idno>
    <idno type="EEBO-CITATION">17194943</idno>
    <idno type="OCLC">ocm 17194943</idno>
    <idno type="VID">106145</idno>
    <idno type="PROQUESTGOID">2240858071</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A52401)</note>
    <note>Transcribed from: (Early English Books Online ; image set 106145)</note>
    <note>Images scanned from microfilm: (Early English books, 1641-1700 ; 1620:35)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>Orders to be observed on the day of the royal coronation of King William and Queen Mary the eleventh of this instant April.</title>
      <author>Norfolk, Henry Howard, Duke of, 1655-1701.</author>
     </titleStmt>
     <extent>1 broadside.</extent>
     <publicationStmt>
      <publisher>Printed by Edward Jones,</publisher>
      <pubPlace>In the Savoy [London] :</pubPlace>
      <date>MDCLXXXIX [1689]</date>
     </publicationStmt>
     <notesStmt>
      <note>Signed: Norfolke and Marshall (i.e. Henry Howard, Duke of Norfolk and marshal of ceremonial protocol).</note>
      <note>Reproduction of original in the Cambridge University Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>William -- III, -- King of England, 1650-1702 -- Coronation.</term>
     <term>Mary -- II, -- Queen of England, 1662-1694 -- Coronation.</term>
     <term>Great Britain -- History -- William and Mary, 1689-1702.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>Orders to be observed on the day of the royal coronation of King William and Queen Mary the eleventh of this instant April.</ep:title>
    <ep:author>England and Wales. Earl Marshal</ep:author>
    <ep:publicationYear>1689</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>301</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2007-12</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-01</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-02</date>
    <label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-02</date>
    <label>Emma (Leeson) Huber</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2008-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A52401-t">
  <body xml:id="A52401-e0">
   <div type="text" xml:id="A52401-e10">
    <pb facs="tcp:106145:1" rend="simple:additions" xml:id="A52401-001-a"/>
    <head xml:id="A52401-e20">
     <w lemma="order" pos="n2" xml:id="A52401-001-a-0010">Orders</w>
     <w lemma="to" pos="prt" xml:id="A52401-001-a-0020">to</w>
     <w lemma="be" pos="vvi" xml:id="A52401-001-a-0030">be</w>
     <w lemma="observe" pos="vvn" xml:id="A52401-001-a-0040">Observed</w>
     <w lemma="on" pos="acp" xml:id="A52401-001-a-0050">On</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-0060">the</w>
     <w lemma="day" pos="n1" xml:id="A52401-001-a-0070">Day</w>
     <w lemma="of" pos="acp" xml:id="A52401-001-a-0080">of</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-0090">the</w>
     <w lemma="royal" pos="j" xml:id="A52401-001-a-0100">ROYAL</w>
     <w lemma="coronation" pos="n1" xml:id="A52401-001-a-0110">CORONATION</w>
     <w lemma="of" pos="acp" xml:id="A52401-001-a-0120">OF</w>
     <w lemma="king" pos="n1" xml:id="A52401-001-a-0130">King</w>
     <w lemma="WILLIAM" pos="nn1" rend="hi" xml:id="A52401-001-a-0140">WILLIAM</w>
     <w lemma="and" pos="cc" xml:id="A52401-001-a-0150">and</w>
     <w lemma="queen" pos="n1" xml:id="A52401-001-a-0160">Queen</w>
     <hi xml:id="A52401-e40">
      <w lemma="MARY" pos="nn1" xml:id="A52401-001-a-0170">MARY</w>
      <pc unit="sentence" xml:id="A52401-001-a-0180">.</pc>
     </hi>
    </head>
    <opener xml:id="A52401-e50">
     <dateline xml:id="A52401-e60">
      <date xml:id="A52401-e70">
       <w lemma="the" pos="d" xml:id="A52401-001-a-0190">The</w>
       <w lemma="eleven" pos="ord" xml:id="A52401-001-a-0200">Eleventh</w>
       <w lemma="of" pos="acp" xml:id="A52401-001-a-0210">of</w>
       <w lemma="this" pos="d" xml:id="A52401-001-a-0220">this</w>
       <w lemma="instant" pos="j" xml:id="A52401-001-a-0230">Instant</w>
       <hi xml:id="A52401-e80">
        <w lemma="April" pos="nn1" xml:id="A52401-001-a-0240">April</w>
        <pc unit="sentence" xml:id="A52401-001-a-0250">.</pc>
       </hi>
      </date>
     </dateline>
    </opener>
    <p xml:id="A52401-e90">
     <w lemma="whereas" pos="cs" xml:id="A52401-001-a-0260">WHEREAS</w>
     <w lemma="his" pos="po" xml:id="A52401-001-a-0270">His</w>
     <w lemma="majesty" pos="n1" xml:id="A52401-001-a-0280">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="A52401-001-a-0290">hath</w>
     <w lemma="command" pos="vvn" xml:id="A52401-001-a-0300">Commanded</w>
     <pc xml:id="A52401-001-a-0310">,</pc>
     <w lemma="that" pos="cs" xml:id="A52401-001-a-0320">That</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-0330">the</w>
     <w lemma="church" pos="n1" xml:id="A52401-001-a-0340">Church</w>
     <w lemma="and" pos="cc" xml:id="A52401-001-a-0350">and</w>
     <w lemma="choir" pos="n1" xml:id="A52401-001-a-0360">Choir</w>
     <w lemma="of" pos="acp" xml:id="A52401-001-a-0370">of</w>
     <w lemma="westminster-abby" pos="n1" rend="hi" xml:id="A52401-001-a-0380">Westminster-Abby</w>
     <w lemma="be" pos="vvb" xml:id="A52401-001-a-0390">be</w>
     <w lemma="keep" pos="vvn" xml:id="A52401-001-a-0400">kept</w>
     <w lemma="free" pos="j" xml:id="A52401-001-a-0410">free</w>
     <w lemma="for" pos="acp" xml:id="A52401-001-a-0420">for</w>
     <w lemma="their" pos="po" xml:id="A52401-001-a-0430">Their</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A52401-001-a-0440">Majesties</w>
     <w lemma="proceed" pos="vvg" xml:id="A52401-001-a-0450">Proceeding</w>
     <pc xml:id="A52401-001-a-0460">,</pc>
     <w lemma="this" pos="d" xml:id="A52401-001-a-0470">This</w>
     <w lemma="be" pos="vvz" xml:id="A52401-001-a-0480">is</w>
     <w lemma="to" pos="prt" xml:id="A52401-001-a-0490">to</w>
     <w lemma="give" pos="vvi" xml:id="A52401-001-a-0500">give</w>
     <w lemma="notice" pos="n1" xml:id="A52401-001-a-0510">Notice</w>
     <pc xml:id="A52401-001-a-0520">,</pc>
     <w lemma="that" pos="cs" xml:id="A52401-001-a-0530">That</w>
     <w lemma="no" pos="dx" xml:id="A52401-001-a-0540">no</w>
     <w lemma="person" pos="n1" xml:id="A52401-001-a-0550">Person</w>
     <w lemma="whatsoever" pos="crq" xml:id="A52401-001-a-0560">whatsoever</w>
     <w lemma="be" pos="vvz" xml:id="A52401-001-a-0570">is</w>
     <w lemma="to" pos="prt" xml:id="A52401-001-a-0580">to</w>
     <w lemma="be" pos="vvi" xml:id="A52401-001-a-0590">be</w>
     <w lemma="admit" pos="vvn" xml:id="A52401-001-a-0600">admitted</w>
     <w lemma="within" pos="acp" xml:id="A52401-001-a-0610">within</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-0620">the</w>
     <w lemma="door" pos="n1" xml:id="A52401-001-a-0630">Door</w>
     <w lemma="of" pos="acp" xml:id="A52401-001-a-0640">of</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-0650">the</w>
     <w lemma="choir" pos="n1" reg="Choir" xml:id="A52401-001-a-0660">Quire</w>
     <pc xml:id="A52401-001-a-0670">,</pc>
     <w lemma="but" pos="acp" xml:id="A52401-001-a-0680">but</w>
     <w lemma="such" pos="d" xml:id="A52401-001-a-0690">such</w>
     <w lemma="as" pos="acp" xml:id="A52401-001-a-0700">as</w>
     <w lemma="shall" pos="vmb" xml:id="A52401-001-a-0710">shall</w>
     <w lemma="produce" pos="vvi" xml:id="A52401-001-a-0720">produce</w>
     <w lemma="ticket" pos="n2" xml:id="A52401-001-a-0730">Tickets</w>
     <w lemma="from" pos="acp" xml:id="A52401-001-a-0740">from</w>
     <w lemma="i" pos="pno" xml:id="A52401-001-a-0750">Me</w>
     <pc xml:id="A52401-001-a-0760">,</pc>
     <w lemma="till" pos="acp" xml:id="A52401-001-a-0770">till</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-0780">the</w>
     <w lemma="entrance" pos="n1" xml:id="A52401-001-a-0790">Entrance</w>
     <w lemma="of" pos="acp" xml:id="A52401-001-a-0800">of</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-0810">the</w>
     <w lemma="say" pos="vvd" xml:id="A52401-001-a-0820">said</w>
     <w lemma="proceed" pos="vvg" xml:id="A52401-001-a-0830">Proceeding</w>
     <pc unit="sentence" xml:id="A52401-001-a-0840">.</pc>
    </p>
    <p xml:id="A52401-e110">
     <w lemma="and" pos="cc" xml:id="A52401-001-a-0850">And</w>
     <w lemma="to" pos="prt" xml:id="A52401-001-a-0860">to</w>
     <w lemma="give" pos="vvi" xml:id="A52401-001-a-0870">give</w>
     <w lemma="notice" pos="n1" xml:id="A52401-001-a-0880">Notice</w>
     <pc xml:id="A52401-001-a-0890">,</pc>
     <w lemma="that" pos="cs" xml:id="A52401-001-a-0900">That</w>
     <w lemma="those" pos="d" xml:id="A52401-001-a-0910">those</w>
     <w lemma="who" pos="crq" xml:id="A52401-001-a-0920">who</w>
     <w lemma="shall" pos="vmb" xml:id="A52401-001-a-0930">shall</w>
     <w lemma="have" pos="vvi" xml:id="A52401-001-a-0940">have</w>
     <w lemma="ticket" pos="n2" xml:id="A52401-001-a-0950">Tickets</w>
     <w lemma="from" pos="acp" xml:id="A52401-001-a-0960">from</w>
     <w lemma="i" pos="pno" xml:id="A52401-001-a-0970">Me</w>
     <pc xml:id="A52401-001-a-0980">,</pc>
     <w lemma="for" pos="acp" xml:id="A52401-001-a-0990">for</w>
     <w lemma="place" pos="n2" xml:id="A52401-001-a-1000">Places</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-1010">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1020">the</w>
     <w lemma="gallery" pos="n2" xml:id="A52401-001-a-1030">Galleries</w>
     <w lemma="adjoin" pos="vvg" reg="adjoining" xml:id="A52401-001-a-1040">adjoyning</w>
     <w lemma="to" pos="acp" xml:id="A52401-001-a-1050">to</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1060">the</w>
     <w lemma="choir" pos="n1" reg="Choir" xml:id="A52401-001-a-1070">Quire</w>
     <pc xml:id="A52401-001-a-1080">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A52401-001-a-1090">shall</w>
     <w lemma="not" pos="xx" xml:id="A52401-001-a-1100">not</w>
     <w lemma="be" pos="vvi" xml:id="A52401-001-a-1110">be</w>
     <w lemma="admit" pos="vvn" xml:id="A52401-001-a-1120">admitted</w>
     <w lemma="after" pos="acp" xml:id="A52401-001-a-1130">after</w>
     <w lemma="nine" pos="crd" xml:id="A52401-001-a-1140">Nine</w>
     <w lemma="a" pos="d" xml:id="A52401-001-a-1150">a</w>
     <w lemma="clock" pos="n1" xml:id="A52401-001-a-1160">Clock</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-1170">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1180">the</w>
     <w lemma="morning" pos="n1" xml:id="A52401-001-a-1190">Morning</w>
     <pc unit="sentence" xml:id="A52401-001-a-1200">.</pc>
    </p>
    <p xml:id="A52401-e120">
     <w lemma="and" pos="cc" xml:id="A52401-001-a-1210">And</w>
     <w lemma="further" pos="avc-j" xml:id="A52401-001-a-1220">further</w>
     <w lemma="to" pos="prt" xml:id="A52401-001-a-1230">to</w>
     <w lemma="warn" pos="vvi" xml:id="A52401-001-a-1240">warn</w>
     <w lemma="all" pos="d" xml:id="A52401-001-a-1250">all</w>
     <w lemma="person" pos="n2" xml:id="A52401-001-a-1260">persons</w>
     <w lemma="concern" pos="vvn" xml:id="A52401-001-a-1270">concerned</w>
     <pc xml:id="A52401-001-a-1280">,</pc>
     <w lemma="that" pos="cs" xml:id="A52401-001-a-1290">That</w>
     <w lemma="those" pos="d" xml:id="A52401-001-a-1300">those</w>
     <w lemma="who" pos="crq" xml:id="A52401-001-a-1310">who</w>
     <w lemma="shall" pos="vmb" xml:id="A52401-001-a-1320">shall</w>
     <w lemma="have" pos="vvi" xml:id="A52401-001-a-1330">have</w>
     <w lemma="ticket" pos="n2" xml:id="A52401-001-a-1340">Tickets</w>
     <w lemma="for" pos="acp" xml:id="A52401-001-a-1350">for</w>
     <w lemma="any" pos="d" xml:id="A52401-001-a-1360">any</w>
     <w lemma="of" pos="acp" xml:id="A52401-001-a-1370">of</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1380">the</w>
     <w lemma="gallery" pos="n2" xml:id="A52401-001-a-1390">Galleries</w>
     <w lemma="or" pos="cc" xml:id="A52401-001-a-1400">or</w>
     <w lemma="place" pos="n2" xml:id="A52401-001-a-1410">Places</w>
     <w lemma="belong" pos="vvg" xml:id="A52401-001-a-1420">belonging</w>
     <w lemma="to" pos="acp" xml:id="A52401-001-a-1430">to</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1440">the</w>
     <w lemma="dean" pos="n1" xml:id="A52401-001-a-1450">Dean</w>
     <w lemma="or" pos="cc" xml:id="A52401-001-a-1460">or</w>
     <w lemma="prebendary" pos="n2" xml:id="A52401-001-a-1470">Prebendaries</w>
     <pc xml:id="A52401-001-a-1480">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A52401-001-a-1490">shall</w>
     <w lemma="not" pos="xx" xml:id="A52401-001-a-1500">not</w>
     <w lemma="be" pos="vvi" xml:id="A52401-001-a-1510">be</w>
     <w lemma="admit" pos="vvn" xml:id="A52401-001-a-1520">admitted</w>
     <w lemma="after" pos="acp" xml:id="A52401-001-a-1530">after</w>
     <w lemma="eight" pos="crd" xml:id="A52401-001-a-1540">Eight</w>
     <w lemma="a" pos="d" xml:id="A52401-001-a-1550">a</w>
     <w lemma="clock" pos="n1" xml:id="A52401-001-a-1560">Clock</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-1570">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1580">the</w>
     <w lemma="morning" pos="n1" xml:id="A52401-001-a-1590">Morning</w>
     <pc unit="sentence" xml:id="A52401-001-a-1600">.</pc>
    </p>
    <p xml:id="A52401-e130">
     <w lemma="and" pos="cc" xml:id="A52401-001-a-1610">And</w>
     <w lemma="to" pos="prt" xml:id="A52401-001-a-1620">to</w>
     <w lemma="give" pos="vvi" xml:id="A52401-001-a-1630">give</w>
     <w lemma="notice" pos="n1" xml:id="A52401-001-a-1640">Notice</w>
     <pc xml:id="A52401-001-a-1650">,</pc>
     <w lemma="that" pos="cs" xml:id="A52401-001-a-1660">That</w>
     <w lemma="all" pos="d" xml:id="A52401-001-a-1670">all</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1680">the</w>
     <w lemma="peer" pos="n2" xml:id="A52401-001-a-1690">Peers</w>
     <w lemma="that" pos="cs" xml:id="A52401-001-a-1700">that</w>
     <w lemma="do" pos="vvb" xml:id="A52401-001-a-1710">do</w>
     <w lemma="go" pos="vvi" xml:id="A52401-001-a-1720">go</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-1730">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1740">the</w>
     <w lemma="say" pos="vvd" xml:id="A52401-001-a-1750">said</w>
     <w lemma="proceed" pos="vvg" xml:id="A52401-001-a-1760">Proceeding</w>
     <pc xml:id="A52401-001-a-1770">,</pc>
     <w lemma="be" pos="vvb" xml:id="A52401-001-a-1780">are</w>
     <w lemma="to" pos="prt" xml:id="A52401-001-a-1790">to</w>
     <w lemma="meet" pos="vvi" xml:id="A52401-001-a-1800">meet</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-1810">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1820">the</w>
     <w lemma="house" pos="n1" xml:id="A52401-001-a-1830">House</w>
     <w lemma="of" pos="acp" xml:id="A52401-001-a-1840">of</w>
     <w lemma="lord" pos="n2" xml:id="A52401-001-a-1850">Lords</w>
     <pc xml:id="A52401-001-a-1860">,</pc>
     <w lemma="and" pos="cc" xml:id="A52401-001-a-1870">and</w>
     <w lemma="all" pos="d" xml:id="A52401-001-a-1880">all</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1890">the</w>
     <w lemma="peeress" pos="n2" xml:id="A52401-001-a-1900">Peeresses</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-1910">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-1920">the</w>
     <w lemma="paint" pos="j-vn" xml:id="A52401-001-a-1930">Painted</w>
     <w lemma="chamber" pos="n1" xml:id="A52401-001-a-1940">Chamber</w>
     <w lemma="at" pos="acp" xml:id="A52401-001-a-1950">at</w>
     <hi xml:id="A52401-e140">
      <w lemma="Westminster" pos="nn1" xml:id="A52401-001-a-1960">Westminster</w>
      <pc xml:id="A52401-001-a-1970">,</pc>
     </hi>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-1980">in</w>
     <w lemma="their" pos="po" xml:id="A52401-001-a-1990">their</w>
     <w lemma="robe" pos="n2" xml:id="A52401-001-a-2000">Robes</w>
     <pc xml:id="A52401-001-a-2010">,</pc>
     <w lemma="and" pos="cc" xml:id="A52401-001-a-2020">and</w>
     <w lemma="with" pos="acp" xml:id="A52401-001-a-2030">with</w>
     <w lemma="their" pos="po" xml:id="A52401-001-a-2040">their</w>
     <w lemma="coronet" pos="n2" xml:id="A52401-001-a-2050">Coronets</w>
     <pc xml:id="A52401-001-a-2060">,</pc>
     <w lemma="by" pos="acp" xml:id="A52401-001-a-2070">by</w>
     <w lemma="eight" pos="crd" xml:id="A52401-001-a-2080">Eight</w>
     <w lemma="a" pos="d" xml:id="A52401-001-a-2090">a</w>
     <w lemma="clock" pos="n1" xml:id="A52401-001-a-2100">Clock</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-2110">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-2120">the</w>
     <w lemma="morning" pos="n1" xml:id="A52401-001-a-2130">Morning</w>
     <pc unit="sentence" xml:id="A52401-001-a-2140">.</pc>
    </p>
    <p xml:id="A52401-e150">
     <w lemma="and" pos="cc" xml:id="A52401-001-a-2150">And</w>
     <w lemma="that" pos="cs" xml:id="A52401-001-a-2160">that</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-2170">the</w>
     <w lemma="choir" pos="n1" xml:id="A52401-001-a-2180">Choir</w>
     <w lemma="of" pos="acp" xml:id="A52401-001-a-2190">of</w>
     <hi xml:id="A52401-e160">
      <w lemma="Westminster" pos="nn1" xml:id="A52401-001-a-2200">Westminster</w>
      <pc xml:id="A52401-001-a-2210">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A52401-001-a-2220">and</w>
     <w lemma="all" pos="d" xml:id="A52401-001-a-2230">all</w>
     <w lemma="other" pos="pi2-d" xml:id="A52401-001-a-2240">others</w>
     <w lemma="concern" pos="vvn" reg="concerned" xml:id="A52401-001-a-2250">concern'd</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-2260">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-2270">the</w>
     <w lemma="carry" pos="vvg" xml:id="A52401-001-a-2280">carrying</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-2290">the</w>
     <hi xml:id="A52401-e170">
      <w lemma="royal" pos="j" xml:id="A52401-001-a-2300">Royal</w>
      <w lemma="n/a" pos="fla" xml:id="A52401-001-a-2310">Regalia</w>
     </hi>
     <w lemma="from" pos="acp" xml:id="A52401-001-a-2320">from</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-2330">the</w>
     <hi xml:id="A52401-e180">
      <w lemma="abbey" pos="n1" xml:id="A52401-001-a-2340">Abbey</w>
      <pc xml:id="A52401-001-a-2350">,</pc>
     </hi>
     <w lemma="be" pos="vvb" xml:id="A52401-001-a-2360">are</w>
     <w lemma="to" pos="prt" xml:id="A52401-001-a-2370">to</w>
     <w lemma="be" pos="vvi" xml:id="A52401-001-a-2380">be</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-2390">in</w>
     <hi xml:id="A52401-e190">
      <w lemma="Westminster-hall" pos="nn1" xml:id="A52401-001-a-2400">Westminster-Hall</w>
      <pc xml:id="A52401-001-a-2410">,</pc>
     </hi>
     <w lemma="before" pos="acp" xml:id="A52401-001-a-2420">before</w>
     <w lemma="ten" pos="crd" xml:id="A52401-001-a-2430">Ten</w>
     <w lemma="a" pos="d" xml:id="A52401-001-a-2440">a</w>
     <w lemma="clock" pos="n1" xml:id="A52401-001-a-2450">Clock</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-2460">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-2470">the</w>
     <w lemma="morning" pos="n1" xml:id="A52401-001-a-2480">Morning</w>
     <pc unit="sentence" xml:id="A52401-001-a-2490">.</pc>
    </p>
    <p xml:id="A52401-e200">
     <w lemma="and" pos="cc" xml:id="A52401-001-a-2500">And</w>
     <w lemma="all" pos="d" xml:id="A52401-001-a-2510">all</w>
     <w lemma="other" pos="pi2-d" xml:id="A52401-001-a-2520">others</w>
     <pc xml:id="A52401-001-a-2530">,</pc>
     <w lemma="who" pos="crq" xml:id="A52401-001-a-2540">who</w>
     <w lemma="be" pos="vvb" xml:id="A52401-001-a-2550">are</w>
     <w lemma="to" pos="prt" xml:id="A52401-001-a-2560">to</w>
     <w lemma="go" pos="vvi" xml:id="A52401-001-a-2570">go</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-2580">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-2590">the</w>
     <w lemma="say" pos="vvd" xml:id="A52401-001-a-2600">said</w>
     <w lemma="proceed" pos="vvg" xml:id="A52401-001-a-2610">Proceeding</w>
     <pc xml:id="A52401-001-a-2620">,</pc>
     <w lemma="beside" pos="acp" xml:id="A52401-001-a-2630">besides</w>
     <w lemma="peer" pos="n2" xml:id="A52401-001-a-2640">Peers</w>
     <w lemma="and" pos="cc" xml:id="A52401-001-a-2650">and</w>
     <w lemma="peeress" pos="n2" xml:id="A52401-001-a-2660">Peeresses</w>
     <pc xml:id="A52401-001-a-2670">,</pc>
     <w lemma="be" pos="vvb" xml:id="A52401-001-a-2680">are</w>
     <w lemma="to" pos="prt" xml:id="A52401-001-a-2690">to</w>
     <w lemma="meet" pos="vvi" xml:id="A52401-001-a-2700">meet</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-2710">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-2720">the</w>
     <w lemma="court" pos="n1" xml:id="A52401-001-a-2730">Court</w>
     <w lemma="of" pos="acp" xml:id="A52401-001-a-2740">of</w>
     <w lemma="request" pos="n2" rend="hi" xml:id="A52401-001-a-2750">Requests</w>
     <w lemma="at" pos="acp" xml:id="A52401-001-a-2760">at</w>
     <hi xml:id="A52401-e220">
      <w lemma="Westminster" pos="nn1" xml:id="A52401-001-a-2770">Westminster</w>
      <pc xml:id="A52401-001-a-2780">,</pc>
     </hi>
     <w lemma="by" pos="acp" xml:id="A52401-001-a-2790">by</w>
     <w lemma="eight" pos="crd" xml:id="A52401-001-a-2800">Eight</w>
     <w lemma="a" pos="d" xml:id="A52401-001-a-2810">a</w>
     <w lemma="clock" pos="n1" xml:id="A52401-001-a-2820">Clock</w>
     <w lemma="in" pos="acp" xml:id="A52401-001-a-2830">in</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-2840">the</w>
     <w lemma="morning" pos="n1" xml:id="A52401-001-a-2850">Morning</w>
     <pc unit="sentence" xml:id="A52401-001-a-2860">.</pc>
    </p>
    <closer xml:id="A52401-e230">
     <signed xml:id="A52401-e240">
      <w lemma="Norfolk" pos="nn1" reg="NORFOLK" xml:id="A52401-001-a-2870">NORFOLKE</w>
      <w lemma="and" pos="cc" rend="hi" xml:id="A52401-001-a-2880">and</w>
      <w lemma="Martial" pos="nn1" reg="MARTIAL" xml:id="A52401-001-a-2890">MARSHALL</w>
      <pc unit="sentence" xml:id="A52401-001-a-2900">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A52401-e260">
   <div type="colophon" xml:id="A52401-e270">
    <p xml:id="A52401-e280">
     <w lemma="in" pos="acp" xml:id="A52401-001-a-2910">In</w>
     <w lemma="the" pos="d" xml:id="A52401-001-a-2920">the</w>
     <w lemma="Savoy" pos="nn1" rend="hi" xml:id="A52401-001-a-2930">SAVOY</w>
     <pc xml:id="A52401-001-a-2940">:</pc>
     <w lemma="print" pos="vvn" xml:id="A52401-001-a-2950">Printed</w>
     <w lemma="by" pos="acp" xml:id="A52401-001-a-2960">by</w>
     <hi xml:id="A52401-e300">
      <w lemma="Edward" pos="nn1" xml:id="A52401-001-a-2970">Edward</w>
      <w lemma="Jones" pos="nn1" xml:id="A52401-001-a-2980">Jones</w>
     </hi>
     <pc xml:id="A52401-001-a-2990">:</pc>
     <w lemma="1689" pos="crd" xml:id="A52401-001-a-3000">MDCLXXXIX</w>
     <pc unit="sentence" xml:id="A52401-001-a-3010">.</pc>
    </p>
   </div>
  </back>
 </text>
</TEI>
